package com.flight.FlightBooking;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.flight.airlineserviceimpl.AirLineServiceImpl;
import com.flight.airlineserviceimpl.FlightServiceImpl;
import com.flight.airlineserviceimpl.RouteServiceImpl;
import com.flight.dto.FlightScheduleRequest;
import com.flight.dto.FlightScheduleResponse;
import com.flight.entity.Airport;
import com.flight.entity.Flight;
import com.flight.entity.FlightSchedule;
import com.flight.entity.Route;
import com.flight.enums.LandType;
import com.flight.exceptions.AirLineNotFoundException;
import com.flight.exceptions.FlightNotFoundException;
import com.flight.exceptions.SourceOrDestinationAirportNotFound;
import com.flight.repository.AirportRepository;
import com.flight.repository.FlightRepository;
import com.flight.repository.FlightScheduleRepository;
import com.flight.repository.RouteRepository;

@ExtendWith(SpringExtension.class)
class FlightBookingApplicationTests {

	@InjectMocks
	AirLineServiceImpl airLineService;
	@InjectMocks
	FlightServiceImpl flightService;
	@InjectMocks
	RouteServiceImpl routeService;

	@Mock
	AirportRepository airportRepository;
	@Mock
	FlightRepository flightRepository;
	@Mock
	FlightScheduleRepository flightScheduleRepository;
	@Mock
	RouteRepository routeRepository;

	Flight flightone;
	Flight flighttwo;
	Flight flightthree;
	Route routeone;
	Route routetwo;
	FlightSchedule flightSchedule;
	FlightSchedule flightSchedule2;
	Airport airportone;
	Airport airporttwo;
	List<Flight> flightlist;
	List<Route> routelist;
	List<FlightSchedule> flightschedules;
	List<FlightScheduleResponse> flighresponses;
	FlightScheduleRequest flightScheduleRequest;
	FlightScheduleResponse flighresponse;

	@BeforeEach
	public void setup() {

		flightone = new Flight();
		flightone.setAirlines("JET");
		flightone.setCapacity(50);
		flightone.setFlightid(1);
		flightone.setServicetype("DOMESTIC");
		flighttwo = new Flight();
		flighttwo.setAirlines("KINGFISHER");
		flighttwo.setCapacity(100);
		flighttwo.setFlightid(2);
		flighttwo.setServicetype("DOMESTIC");
		flightthree = new Flight();
		flightthree.setAirlines("KINGFISHER");
		flightthree.setCapacity(100);
		flightthree.setFlightid(3);
		flightthree.setServicetype("INTERNATIONAL");
		routeone = new Route();
		routeone.setAvailableseats(40);
		routeone.setSourceairportid(1);
		routeone.setDestinationaiportid(2);
		routetwo = new Route();
		routetwo.setAvailableseats(40);
		routetwo.setSourceairportid(1);
		routetwo.setDestinationaiportid(2);
		flightSchedule = new FlightSchedule();
		flightSchedule.setAirportid(1);
		flightSchedule.setFlightid(1);
		flightSchedule.setdatetime(LocalDateTime.now());
		flightSchedule.setLandtype(LandType.LANDING);
		flightSchedule2 = new FlightSchedule();
		flightSchedule2.setAirportid(2);
		flightSchedule2.setFlightid(2);
		flightSchedule2.setdatetime(LocalDateTime.now());
		flightSchedule2.setLandtype(LandType.LANDING);
		airportone=new Airport();
		airporttwo=new Airport();
		airportone.setAddress("Hyderabad");
		airportone.setAirportid(1);
		airportone.setAirportname("shamshabad");
		airportone.setNoofrunways("one");
        airporttwo.setAddress("Bengaluru");
		airporttwo.setAirportid(2);
		airporttwo.setAirportname("kgowda");
		airporttwo.setNoofrunways("three");

	}

	@Test
	public void findFlightbyairlinenegitive() throws AirLineNotFoundException {
		flightlist = new ArrayList<>();
		Mockito.when(flightRepository.findAllByAirlines("JET")).thenReturn(flightlist);
		// airLineService.getallflights("JET");
		assertThrows(AirLineNotFoundException.class, () -> airLineService.getallflights("JET"));

	}

	@Test
	public void findFlightbyairlinepositive() throws AirLineNotFoundException {
		flightlist = new ArrayList<>();
		flightlist.add(flightone);
		flightlist.add(flighttwo);
		Mockito.when(flightRepository.findAllByAirlines("JET")).thenReturn(flightlist);
		// airLineService.getallflights("JET");
		assertEquals(2, airLineService.getallflights("JET").size());

	}

	@Test
	public void findFlightbycurrentschedulednegitive() throws FlightNotFoundException, DateTimeParseException {
		flightschedules = new ArrayList<>();
		flightScheduleRequest = new FlightScheduleRequest();
		flightScheduleRequest.setAirportid(1);
		flightScheduleRequest.setFlightid(1);
		String date = LocalDateTime.now().toLocalDate().toString();
		System.out.println(date);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(date + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(date + " 23:59:59", formatter);
		flightScheduleRequest.setDate(LocalDateTime.now().toLocalDate());
		Mockito.when(flightScheduleRepository.findAllByAirportidAndFlightidAndDatetimeBetween(
				flightScheduleRequest.getAirportid(), flightScheduleRequest.getFlightid(), timeone, timetwo))
				.thenReturn(flightschedules);
		assertThrows(FlightNotFoundException.class,
				() -> flightService.findcurrentscheduledflights(flightScheduleRequest));

	}

	@Test
	public void findFlightbycurrentscheduledpositive() throws FlightNotFoundException, DateTimeParseException {
		flightschedules = new ArrayList<>();
		flighresponses = new ArrayList<>();
		flightScheduleRequest = new FlightScheduleRequest();
		flighresponse = new FlightScheduleResponse();
		flightScheduleRequest.setAirportid(1);
		flightScheduleRequest.setFlightid(1);
		String date = LocalDateTime.now().toLocalDate().toString();
		flightschedules.add(flightSchedule);
		// flightschedules.add(flightSchedule2);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(date + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(date + " 23:59:59", formatter);
		flightScheduleRequest.setDate(LocalDateTime.now().toLocalDate());
		Mockito.when(flightScheduleRepository.findAllByAirportidAndFlightidAndDatetimeBetween(
				flightScheduleRequest.getAirportid(), flightScheduleRequest.getFlightid(), timeone, timetwo))
				.thenReturn(flightschedules);
		Mockito.when(flightRepository.findByFlightid(1)).thenReturn(flightone);
		flighresponse.setAirline("Jet");
		flighresponse.setAirportid(1);
		flighresponse.setFlightid(1);
		flighresponse.setLandtype(LandType.LANDING.name());
		flighresponse.setDate(LocalDate.now());
		flighresponses.add(flighresponse);
		flightService.findcurrentscheduledflights(flightScheduleRequest);
		flightService.findcurrentscheduledflights(flightScheduleRequest);
		assertEquals(1, flightService.findcurrentscheduledflights(flightScheduleRequest).getBody().size());

	}

	@Test
	public void findFlightbyroutesnegitive() throws FlightNotFoundException {
		String source = "Hyderabad";
		String destination = "Bengaluru";
		String date = "2020-03-25";
		Airport aone = new Airport();
		Mockito.when(airportRepository.findByAddressLike("%" + source + "%")).thenReturn(aone);

		assertThrows(SourceOrDestinationAirportNotFound.class,
				() -> routeService.getflightsfromroutes(source, destination, date));

	}

	@Test
	public void findFlightbyroutespositive() throws FlightNotFoundException, SourceOrDestinationAirportNotFound {
		String source = "Hyderabad";
		String destination = "Bengaluru";
		String date = "2020-03-25";
		routelist = new ArrayList<>();
		routeone.setFlight(flightone);
		routeone.setRouteid(1);
		routetwo.setFlight(flighttwo);
		routetwo.setRouteid(2);
		routelist.add(routeone);
		//routelist.add(routetwo);
		FlightSchedule fs = new FlightSchedule();
		Optional<FlightSchedule> flightschedule = Optional.of(fs);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(date + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(date + " 23:59:59", formatter);
		Mockito.when(airportRepository.findByAddressLike("%" + source + "%")).thenReturn(airportone);
		Mockito.when(airportRepository.findByAddressLike("%" + destination + "%")).thenReturn(airportone);
		Mockito.when(routeRepository.findAllBySourceairportidAndDestinationaiportid(Mockito.anyInt(), Mockito.anyInt())).thenReturn(routelist);
	
		Mockito.when(flightScheduleRepository.findByAirportidAndFlightidAndDatetimeBetween(1, 1, timeone, timetwo)).thenReturn(flightschedule);
		
	assertEquals(1, routeService.getflightsfromroutes(source, destination, date).getBody().size());
	}
	@Test
	public void findFlightbyroutesnegitivetwo() throws FlightNotFoundException, SourceOrDestinationAirportNotFound {
		String source = "Hyderabad";
		String destination = "Bengaluru";
		String date = "2020-03-25";
		routelist = new ArrayList<>();
		routeone.setFlight(flightone);
		routeone.setRouteid(1);
		routetwo.setFlight(flighttwo);
		routetwo.setRouteid(2);
		routelist.add(routeone);
		routelist.add(routetwo);
		FlightSchedule fs = new FlightSchedule();
		Optional<FlightSchedule> flightschedule = Optional.of(fs);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(date + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(date + " 23:59:59", formatter);
		Mockito.when(airportRepository.findByAddressLike("%" + source + "%")).thenReturn(airportone);
		Mockito.when(airportRepository.findByAddressLike("%" + destination + "%")).thenReturn(airportone);
		Mockito.when(routeRepository.findAllBySourceairportidAndDestinationaiportid(Mockito.anyInt(), Mockito.anyInt())).thenReturn(routelist);
	     Mockito.when(flightScheduleRepository.findByAirportidAndFlightidAndDatetimeBetween(1, 1, timeone, timetwo)).thenReturn(flightschedule);
		assertThrows(FlightNotFoundException.class, () -> routeService.getflightsfromroutes(source, destination, date));
		
	}

}
