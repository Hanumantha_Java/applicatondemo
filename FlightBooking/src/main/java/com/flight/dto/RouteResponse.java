package com.flight.dto;

import java.time.LocalDateTime;

public class RouteResponse {
	
	private String source;
	private String destination;
	private int flightid;
	private int availableseats;
	private LocalDateTime datetime;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getFlightid() {
		return flightid;
	}
	public void setFlightid(int flightid) {
		this.flightid = flightid;
	}
	public int getAvailableseats() {
		return availableseats;
	}
	public void setAvailableseats(int availableseats) {
		this.availableseats = availableseats;
	}
	public LocalDateTime getDatetime() {
		return datetime;
	}
	public void setDatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}
	public RouteResponse(String source, String destination, int flightid, int availableseats, LocalDateTime datetime) {
		super();
		this.source = source;
		this.destination = destination;
		this.flightid = flightid;
		this.availableseats = availableseats;
		this.datetime = datetime;
	}
	public RouteResponse() {
		//create object
	}
	
	

}
