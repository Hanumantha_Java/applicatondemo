package com.flight.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.entity.Airport;

public interface AirportRepository extends JpaRepository<Airport, Integer>{

	Airport findByAddressLike(String string);

}
