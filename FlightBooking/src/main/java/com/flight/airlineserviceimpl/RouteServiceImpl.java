package com.flight.airlineserviceimpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.flight.airlineservice.RouteService;
import com.flight.dto.RouteResponse;
import com.flight.entity.Airport;
import com.flight.entity.FlightSchedule;
import com.flight.entity.Route;
import com.flight.exceptions.FlightNotFoundException;
import com.flight.exceptions.SourceOrDestinationAirportNotFound;
import com.flight.repository.AirportRepository;
import com.flight.repository.FlightScheduleRepository;
import com.flight.repository.RouteRepository;

@Service
public class RouteServiceImpl implements RouteService{
	
	@Autowired
	RouteRepository routeRepository;
	@Autowired
	AirportRepository airportRepository;
	@Autowired
	FlightScheduleRepository flightScheduleRepository;
	
	//find flights basesd on source and destination
	
	public ResponseEntity<List<RouteResponse>> getflightsfromroutes(String source,String destination,String date) throws FlightNotFoundException,SourceOrDestinationAirportNotFound
	{
		RouteResponse routeresponses=new RouteResponse();
		Airport sourceairport=airportRepository.findByAddressLike("%"+source+"%");
		Airport destinationairport=airportRepository.findByAddressLike("%"+destination+"%");
		List<RouteResponse> routeresponse=new ArrayList<>();
		if(Objects.isNull(sourceairport)||Objects.isNull(destinationairport)) {
			throw new SourceOrDestinationAirportNotFound("no airports for given source or destination location");
		}
		else
		{
			List<Route> routes=routeRepository.findAllBySourceairportidAndDestinationaiportid(sourceairport.getAirportid(),destinationairport.getAirportid());
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime timeone = LocalDateTime.parse(date + " 00:00:01", formatter);
			LocalDateTime timetwo = LocalDateTime.parse(date + " 23:59:59", formatter);
			
			for (Route temp : routes) 
			{	
			
			Optional<FlightSchedule> flightSchedule =flightScheduleRepository.findByAirportidAndFlightidAndDatetimeBetween(temp.getSourceairportid(),temp.getFlight().getFlightid(),timeone,timetwo);
			if(flightSchedule.isPresent())
			{
			routeresponses.setFlightid(flightSchedule.get().getFlightid());
			routeresponses.setSource(sourceairport.getAddress());
			routeresponses.setDestination(destinationairport.getAddress());
			routeresponses.setAvailableseats(temp.getAvailableseats());
			routeresponses.setDatetime(flightSchedule.get().getdatetime());
			routeresponse.add(routeresponses);
			}
			else
			{
				throw new FlightNotFoundException("no flighs schedule on given source airport");
			}
			}
			 return new ResponseEntity<>(routeresponse,HttpStatus.OK);
		}
	}

}
