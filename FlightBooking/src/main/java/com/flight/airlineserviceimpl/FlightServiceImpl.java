package com.flight.airlineserviceimpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.flight.airlineservice.FlightService;
import com.flight.dto.FlightScheduleRequest;
import com.flight.dto.FlightScheduleResponse;
import com.flight.entity.FlightSchedule;
import com.flight.exceptions.FlightNotFoundException;
import com.flight.repository.FlightRepository;
import com.flight.repository.FlightScheduleRepository;
@Service
public class FlightServiceImpl implements FlightService{
	
	@Autowired
	FlightScheduleRepository flightScheduleRepository;
	@Autowired
	FlightRepository flightRepository;
	
	//get all flight details based on scheduled flights on particular airport
	
	public ResponseEntity<List<FlightScheduleResponse>> findcurrentscheduledflights(FlightScheduleRequest flightScheduleRequest) throws FlightNotFoundException
	{
		List<FlightScheduleResponse> flighresponse=new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(flightScheduleRequest.getDate() + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(flightScheduleRequest.getDate() + " 23:59:59", formatter);
		List<FlightSchedule> flightSchedule =flightScheduleRepository.findAllByAirportidAndFlightidAndDatetimeBetween(flightScheduleRequest.getAirportid(),flightScheduleRequest.getFlightid(),timeone,timetwo);
	
		if(flightSchedule.isEmpty())
		{
		throw new FlightNotFoundException("no Flights available");
		}
		else
		{
			
			for (FlightSchedule temp : flightSchedule) 
			{	
		        FlightScheduleResponse flightres=new FlightScheduleResponse();
				flightres.setAirportid(temp.getAirportid());
				flightres.setFlightid(temp.getFlightid());
				flightres.setLandtype(temp.getLandtype().name());
				flightres.setAirline(flightRepository.findByFlightid(temp.getFlightid()).getAirlines());
				flightres.setCapacity(flightRepository.findByFlightid(temp.getFlightid()).getCapacity());
				flightres.setDate(temp.getdatetime().toLocalDate());
				flighresponse.add(flightres);
			}
			return new ResponseEntity<>(flighresponse,HttpStatus.OK);
		}
	}

}
