package com.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Airport {
	
	
	@Id
	@GeneratedValue
	private int airportid;
	private String airportname;
	private String address;
	private String noofrunways;
	
	public int getAirportid() {
		return airportid;
	}
	public void setAirportid(int airportid) {
		this.airportid = airportid;
	}
	public String getAirportname() {
		return airportname;
	}
	public void setAirportname(String airportname) {
		this.airportname = airportname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNoofrunways() {
		return noofrunways;
	}
	public void setNoofrunways(String noofrunways) {
		this.noofrunways = noofrunways;
	}
	public Airport(int airportid, String airportname, String address, String noofrunways) {
		super();
		this.airportid = airportid;
		this.airportname = airportname;
		this.address = address;
		this.noofrunways = noofrunways;
	}
	public Airport() {
		//create object
	}
	
	
}
