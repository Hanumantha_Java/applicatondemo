package com.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Route {
	
	@Id
	@GeneratedValue
	private int routeid;
	private int sourceairportid;
	private int destinationaiportid;
	
	private int availableseats;
	public int getRouteid() {
		return routeid;
	}
	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}

public int getAvailableseats() {
		return availableseats;
	}
	public void setAvailableseats(int availableseats) {
		this.availableseats = availableseats;
	}




	public int getSourceairportid() {
		return sourceairportid;
	}
	public void setSourceairportid(int sourceairportid) {
		this.sourceairportid = sourceairportid;
	}
	public int getDestinationaiportid() {
		return destinationaiportid;
	}
	public void setDestinationaiportid(int destinationaiportid) {
		this.destinationaiportid = destinationaiportid;
	}




	public Route(int routeid, int sourceairportid, int destinationaiportid, int availableseats) {
		super();
		this.routeid = routeid;
		this.sourceairportid = sourceairportid;
		this.destinationaiportid = destinationaiportid;
		this.availableseats = availableseats;
	}




	public Route() {
		//create object
	}




	@ManyToOne
	@JoinColumn(name="fk_flight")
	private Flight flight;
	
	public Route(Flight flight) {
		super();
		this.flight = flight;
	}
	public Flight getFlight() {
		return flight;
	}
	public void setFlight(Flight flight) {
		this.flight = flight;
	}

}
