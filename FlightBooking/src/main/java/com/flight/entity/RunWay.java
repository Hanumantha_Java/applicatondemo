package com.flight.entity;

import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.OneToOne;

import com.flight.enums.FlightRunWay;

@Entity
public class RunWay {
	
	@Id
	@GeneratedValue
	private int runwayid;
	@Enumerated(EnumType.STRING)
	private FlightRunWay flightrunway;
	private LocalTime time;
	int getRunwayid() {
		return runwayid;
	}
	void setRunwayid(int runwayid) {
		this.runwayid = runwayid;
	}
	FlightRunWay getFlightrunway() {
		return flightrunway;
	}
	void setFlightrunway(FlightRunWay flightrunway) {
		this.flightrunway = flightrunway;
	}
	LocalTime getTime() {
		return time;
	}
	void setTime(LocalTime time) {
		this.time = time;
	}
	public RunWay(int runwayid, FlightRunWay flightrunway, LocalTime time) {
		super();
		this.runwayid = runwayid;
		this.flightrunway = flightrunway;
		this.time = time;
	}
	public RunWay() {
		//create object
	}
	
@OneToOne
@JoinColumn(name="fk_flightschedule")
private FlightSchedule flightSchedule;
public RunWay(FlightSchedule flightSchedule) {
	super();
	this.flightSchedule = flightSchedule;
}
FlightSchedule getFlightSchedule() {
	return flightSchedule;
}
void setFlightSchedule(FlightSchedule flightSchedule) {
	this.flightSchedule = flightSchedule;
}

}
